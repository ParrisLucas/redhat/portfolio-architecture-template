Portfolio Architecture Blueprint Template
=========================================

This is a beginners guide to how portfolio architecture blueprints are put together. Follow along and learn step-by-step how to
design, create, and publish your architectural blueprints.

See online guide to get started: [Beginners guide](https://redhatdemocentral.gitlab.io/portfolio-architecture-template)

[![Cover](https://gitlab.com/redhatdemocentral/portfolio-architecture-template/-/raw/master/cover.png)](https://redhatdemocentral.gitlab.io/portfolio-architecture-template)

